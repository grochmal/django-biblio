django biblio - storage model for BibTeX references
===================================================

Django app with data models and admin interface to deal with projects involving
huge amounts of references.  The references of works (papers, books, conference
reports) can be imported and exported in BibTeX format, on a work or project
basis.

Copying
-------

Copyright (C) 2014 Michal Grochmal

This file is part of the django biblio (djbiblio) package.

django biblio is free software; you can redistribute and/or modify all or part
of the code under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or (at your
option) any later version.

django biblio is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

The COPYING file in the root directory of the project contains a copy of the
GNU General Public License. If you cannot find this file, see
<http://www.gnu.org/licenses/>.

